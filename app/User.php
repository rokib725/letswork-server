<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   

   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','special_user','contact','username','role_id','user_id','company_id'
        //,'role_id','username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
     public function get_users($parentid,$searchterm='',$searchid='id',$order='DESC'  )
    {
    $escaped = '%' . $searchterm . '%';
     $sql="select * from users where  (users.name like '$escaped' or users.email like '$escaped' or users.username like '$escaped')" ;

    $results = DB::select($sql);
     
    
    return $results;

    }
     
    /*public function username()
{
    return 'username';
}*/
public function store_online_user($request)
    {
       

        $input['name']=$request['name'];
        $input['username']=$request['username'];
         $input['contact']=$request['contact'];
        $input['email']=$request['email'];
        $input['password'] = Hash::make($request['password']);
        $input['status']=1;
        $input['special_user']='Y';
        
        $user_company=array();
        
        
        DB::beginTransaction();
        try {
        $user = User::create($input);  
          $user->attachRole(2); 
          $all_company[0]=22;
          $all_company[1]=23;
           $all_company[2]=24;
          $all_company[3]=25;
       /* foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }*/
        $v=0;
        foreach ($all_company as $key => $value) {
            $user_company[$v]['company_id']=$value;
            $user_company[$v]['user_id']=$user->id;
             $user_company[$v]['status']=1;
             $user_company[$v]['insert_by']=1;
             $v++;
          
            
        }
        $updaterow = CompanyUser::insert($user_company);
        
        DB::commit();
        return true;
        } catch (\Throwable $e) {
   DB::rollback();
    return false;
}
         
        
        
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }
}
