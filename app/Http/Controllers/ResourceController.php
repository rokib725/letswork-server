<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Resource;

class ResourceController extends Controller
{
    
    public function showAll(Request $request) {
        $title = "Resource ";        
        
        return view('pages.admin.resource.resource')->with(compact('title'));
    }
    
    public function saveResourceCategory(Request $request) {
       $title = "Resource Category"; 
       $single_resource=array();
       //save new resource category
    if(isset($request->category_name) && isset($request->categoryid) && ($request->categoryid=='0'))
      {
      	
       $resource = new Resource();
       $resource->category_name=$request->category_name;
       $resource->category_detail=$request->category_detail;
      	
       $resource->status=1;
       $resource->insert_by=session('user_id');
       $resource->last_update_by=session('user_id');

       $resource->save();
       }
       //edit or delete resource category by status field
     else if(isset($request->categoryid) && !isset($request->category_name))
      {
      	//edit resource category 
      	 if ($request->status=='1') {
      	 	$resource=new Resource();
	        $resource=$resource->get_resource('',$request->categoryid);
	        $single_resource=(array)$resource[0];
      	 }
      	 //delete resource category 
      	 else{
      	 	   $resource = Resource::find($request->categoryid);
		       $resource->status=0;
		       $resource->last_update_by=session('user_id');
		       $resource->save();
      	    }      


       }
       //update resource category 
       else if(isset($request->categoryid) && isset($request->category_name) && ($request->categoryid!='0'))
       {     

       $resource = Resource::find($request->categoryid); 	  
       $resource->category_name=$request->category_name;
       $resource->category_detail=$request->category_detail;              
       $resource->last_update_by=session('user_id');
       $resource->save();

       }

       $resources = Resource::all()->where('status',1);       
       return view('pages.admin.resource.category')->with(compact('title','resources','single_resource'));

    }
}
