<?php

namespace App\Http\Controllers;

//use App\Messaging;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PackageController extends Controller
{
    
    public function createNew(Request $request) {
        $title = "Create";        
        
        return view('pages.admin.package.create')->with(compact('title'));
    }
     public function showAll(Request $request) {
        $title = "Bank Details";        
        
        return view('pages.admin.package.all')->with(compact('title'));
    }
}
