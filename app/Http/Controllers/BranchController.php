<?php

namespace App\Http\Controllers;

//use App\Messaging;
use Illuminate\Http\Request;
//use App\Accounts;
//use App\InventorySummary;
use Illuminate\Support\Facades\Auth;
//use App\MessagingDocument;
use App\Branch;
use App\Resource;
use App\BranchResource;


class BranchController extends Controller
{
    
    public function createNew(Request $request) {
        $title = "Create Branch";
        $branches = Branch::all()->where('status',1); 
        $resources = Resource::all()->where('status',1);        
        return view('pages.admin.branch.create')->with(compact('title','branches','resources'));

    }
    public function saveNew(Request $request) {
       $title = "Save Branch"; 

       $branch = new Branch();
       $branch->branch_name=$request->branch_name;
       $branch->manager_name=$request->manager_name;
       $branch->phone=$request->phone;
       $branch->email=$request->email;
       $branch->address= $request->address;
       $branch->status=1;
       $branch->insert_by=session('user_id');
       $branch->last_update_by=session('user_id');

       $branch->save();

       
       foreach ($request->resource_category as $key => $value) {
            $branch_resource = new BranchResource();
            $branch_resource->branch_id = $branch->branch_id;
            $branch_resource->resource_category_id = $key;
            $branch_resource->resource_quantity = $value;
            $branch_resource->status=1;
            $branch_resource->insert_by=session('user_id');
            $branch_resource->last_update_by=session('user_id');
            $branch_resource->save();
       }

       $branches = Branch::all();  
       $resources = Resource::all()->where('status',1);      
       return view('pages.admin.branch.create')->with(compact('title','branches','resources'));

    }
     public function editBranch(Request $request) {

      $branch=new Branch();
      $branch=$branch->get_branch($request->branch_id);
      $branch=$branch[0];
      $resources = BranchResource::all()->where('branch_id',$request->branch_id); 
      

      $title = "Edit Branch";
        
      return view('pages.admin.branch.edit')->with(compact('title','branch','resources'));        

    }
     public function updateBranch(Request $request) {
      
       $branch = Branch::find($request->branch_id);
       $branch->branch_name=$request->branch_name;
       $branch->manager_name=$request->manager_name;
       $branch->phone=$request->phone;
       $branch->email=$request->email;
       $branch->address= $request->address;       
       $branch->last_update_by=session('user_id');

       $branch->save();
       return redirect('/branches');
    }
    public function deleteBranch(Request $request) {
      
       $branch = Branch::find($request->branch_id);
       $branch->status=0;
       $branch->last_update_by=session('user_id');
       $branch->save();
       return redirect('/branches');
    }
     public function showAll(Request $request) {
        $title = "Bank Details";        
        $branches = Branch::all()->where('status',1);       
        return view('pages.admin.branch.all')->with(compact('title','branches'));
       
    }
}
