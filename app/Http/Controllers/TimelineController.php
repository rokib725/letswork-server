<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TimelineController extends Controller
{
    
    public function timeLine(Request $request) {
        $title = "Timeline ";         
        
        return view('pages.admin.timeline.timeline')->with(compact('title'));
    }
}
