<?php

namespace App\Http\Controllers;

//use App\Messaging;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MessageController extends Controller
{
    
    public function composeMessage(Request $request) {
        $title = "Bank Details";
         
        
        return view('pages.admin.message.compose')->with(compact('title'));
    }
    public function inboxMessage(Request $request) {
        $title = "Bank Details";
         
        
        return view('pages.admin.message.inbox')->with(compact('title'));
    }
    public function messageDetails(Request $request) {
        $title = "Bank Details";
         
        
        return view('pages.admin.message.details')->with(compact('title'));
    }
}
