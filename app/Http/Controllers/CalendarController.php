<?php

namespace App\Http\Controllers;

//use App\Messaging;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CalendarController extends Controller
{
    
    public function calendar(Request $request) {
        $title = "Calendar";
         
        
        return view('pages.admin.calendar.calendar')->with(compact('title'));
    }
}
