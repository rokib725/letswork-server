<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MemberController extends Controller
{
    
    public function createNew(Request $request) {
        $title = "Create";        
        
        return view('pages.admin.member.create')->with(compact('title'));
    }
     public function showAll(Request $request) {
        $title = "Bank Details";        
        
        return view('pages.admin.member.all')->with(compact('title'));
    }
    public function memberDetail(Request $request) {
        $title = "Bank Details";        
        
        return view('pages.admin.member.details')->with(compact('title'));
    }
    public function memberBalance(Request $request) {
        $title = "Bank Details";        
        
        return view('pages.admin.member.balance')->with(compact('title'));
    }
}
