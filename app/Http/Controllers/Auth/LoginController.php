<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User; 
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    public function welcome() {
        $title = "Login";
       //return view('auth.login');
        return view('auth.login')->with(compact('title'));
    }
    protected $loginPath = '/welcome';
    
    
    private function loginPath()
    {
        return "/login";
    }
    public function postLogin(Request $request)
    {


    $users=new User();
    $user=$users->get_users('',$request->username);
      
     //print_r($user);
     if (!empty($user)) {

         session(['user_id' => $user[0]->id]);
         return redirect('/home');
       
     }
            
    }
}
