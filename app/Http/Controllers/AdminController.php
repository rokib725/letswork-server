<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    
    public function dashboard(Request $request) {
        $title = "Bank Details";
         
        
        return view('pages.admin.dashboard')->with(compact('title'));
    }
     public function appbrowser(Request $request) {
        $title = "Bank Details";
         
        
        return view('pages.admin.appbrowser')->with(compact('title'));
    }
}
