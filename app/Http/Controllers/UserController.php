<?php
namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Company;
use App\CompanyUser;
use DB;
use Hash;
use  Illuminate\Support\Facades\Auth;
use Config;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }*/
    
    
     public function index(Request $request)
    {
      
            
            
    }
    private function get_sort_option()
    {
		$sort['name']='NAME';
		$sort['email']='EMAIL';
		$sort['username']='USERNAME';
		
		return $sort;
	}
    private function get_users($companyid,$searchterm='',$searchid='id',$order='DESC'  )
    {
	
	
	          
    $results = User::where('id', 'in', '(select user_id from ent_company_user c where c.`status`=1
and ( c.company_id in (select y.company_id from ent_company y where y.parent_id='.$companyid.') or c.company_id='.$companyid.') )')
    ->where('name', 'LIKE', '%'. $searchterm .'%')
    ->orWhere('email', 'LIKE', '%'. $searchterm .'%')
     ->orWhere('username', 'LIKE', '%'. $searchterm .'%')
     ->orderBy($searchid,$order)
     ->paginate(Config::get('constants.paginate'));
     
    
    return $results;

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = Role::pluck('display_name','id');
        if($request->session()->get('companyid')==1)
        {
		$company = Company::where('status',1)
        ->pluck('company_name', 'company_id');
		}else
		{
			$company = Company::where('company_id', $request->session()->get('companyid'))
        ->orWhere('parent_id',$request->session()->get('companyid'))
        ->pluck('company_name', 'company_id');
		}
        
        
        return view('pages/admin/users/create',compact('roles','company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['status']=1;
        
        $user_company=array();
        
        
        DB::beginTransaction();
        try {
        $user = User::create($input);  
           
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        $v=0;
        foreach ($request->input('company') as $key => $value) {
            $user_company[$v]['company_id']=$value;
            $user_company[$v]['user_id']=$user->id;
             $user_company[$v]['status']=1;
             $user_company[$v]['insert_by']=Auth::id();
             $v++;
          
            
        }
        $updaterow = CompanyUser::insert($user_company);
        
        DB::commit();
        } catch (\Throwable $e) {
   DB::rollback();
    throw $e;
}
         
        
        
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $user = User::find($id);
        $roles = Role::pluck('display_name','id');
        $userRole = $user->roles->pluck('id','id')->toArray();
        $company_user=new CompanyUser();
       $selected_company=$company_user->get_company_users($id);
       $company=array();
       foreach($selected_company as $c){
	   	array_push($company,$c->company_id);
	   }
        $all_company = Company::where('company_id', $request->session()->get('companyid'))
        ->orWhere('parent_id',$request->session()->get('companyid'))
        ->pluck('company_name', 'company_id');

        return view('pages/admin/users/edit',compact('user','roles','userRole','company','all_company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('role_user')->where('user_id',$id)->delete();

        
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        
         $v=0;
        foreach ($request->input('company') as $key => $value) {
            $user_company[$v]['company_id']=$value;
            $user_company[$v]['user_id']=$user->id;
             $user_company[$v]['status']=1;
             $user_company[$v]['insert_by']=Auth::id();
             $v++;
          
            
        }
        DB::table('ent_company_user')
            ->where('user_id', $user->id)
            ->update(['status' => 2]);
        $updaterow = CompanyUser::insert($user_company);

        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}
?>