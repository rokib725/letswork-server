<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;
class BranchResource extends Model
{
    
   

    protected $table = 'xyz_branch_resource';
   // protected $primaryKey = 'branch_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id', 'resource_category_id', 'resource_quantity', 'status', 'insert_by', 'insert_date', 'updated_at', 'created_at'
    ];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'last_update_date';
     



}
?>
