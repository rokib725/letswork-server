<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;
class Branch extends Model
{
    
   

    protected $table = 'xyz_branch';
    protected $primaryKey = 'branch_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_name', 'manager_name', 'phone', 'email', 'address', 'status', 'insert_by', 'insert_date', 'updated_at', 'created_at'
    ];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'last_update_date';
     

    
    
public function saveBranch($request)
    {
       

        $input['branch_name']=['branch_name'];
        $input['manager_name']=['manager_name'];
        $input['phone']=['phone'];
        $input['email']=['email'];
        $input['address'] = ['address'];
        $input['status']=1;
        $input['insert_by']=1;
        $input['insert_date']='';
        
        $user = Branch::create($input); 
        
        return redirect()->route('branches.create')
                        ->with('success','User created successfully');
    }

 public function get_branch($id='' )
    {
    
     $sql="select * from xyz_branch where branch_id= ".$id ;

    $results = DB::select($sql);
     
    
    return $results;

    }

}
?>
