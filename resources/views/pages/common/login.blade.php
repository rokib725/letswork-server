@extends('layouts.login')
@section('content')
<div class="content">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                    	
                    
                 {!! Form::open(array('route' => 'auth.login','method'=>'POST')) !!} 
                 <meta name="csrf-token" content="{{ csrf_token() }}" />      

                            <div class="card" id="easylog">
                                <div class="text-center">
                                	<a href="#"><img src="./entromizer-ui/img/Logo.jpg" alt=""></a>                                 
                                    <h2 style="margin:0px;">@lang('common.login_keyword')</h2>
                                </div>
                                <div class="content">
                                    <div class="form-group">
                                        <label>@lang('common.user_name')</label>
                                        <input type="text" placeholder="User Name" name="username" id="username" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('common.password_keyword')</label>
                                        <input type="password" placeholder="Password" name="password" id="password" class="form-control" required>
                                    </div>                                    
                                    <div class="form-group">
                                        @if ($message = Session::get('error'))
                                        <div class="alert alert-danger">
                                            <p>{{ $message }}</p>
                                        </div>
                                    	@endif
                                    </div>
                                    
                                    <div class="row">
                                    	<div class="col-md-8 col-sm-8 col-xs-8">
                                        	<div class="form-group">
                                                <label class="checkbox" style="font-size:11px;">
                                                    <input type="checkbox" data-toggle="checkbox" value="">
                                                    @lang('common.stay_login')
                                                </label>    
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                        	<p style="font-size:10px; line-height:35px;"><a href="javascript:;" class="forgotpasswd text-danger">@lang('common.forgot_pass')</a></p>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="footer text-center">                                	
                                    <button type="submit" class="btn btn-fill btn-info btn-wd">@lang('common.login_keyword')</button>
                                </div>
                            </div>
                            {!! Form::close() !!}  
                            {!! Form::open(array('route' => 'password.email','method'=>'POST')) !!} 
                            <meta name="csrf-token" content="{{ csrf_token() }}" />
                            <div class="card" id="forgotlog" style="display:none;">
                                <div class="text-center">
                                	<a href="#"><img src="./entromizer-ui/img/Logo.jpg" alt=""></a>                                 
                                    <p style="margin:10px 0px;"> <a href="javascript:;" class="backlogin text-danger"><i class="pe-7s-back"></i> @lang('common.back_to_login')</a></p>
                                </div>
                                <div class="content">
                                    <div class="form-group">
                                        <label>@lang('common.enter_email')</label>
                                        <input type="text" placeholder="" name="email" id="email" class="form-control" required>
                                    </div>                                    
                                </div>
                                
                                
                                <div class="footer text-center">                                	
                                    <button type="submit" class="btn btn-fill btn-info btn-wd">@lang('common.submit_keyword')</button>
                                </div>
                            </div>
                                
                           
                                
                     {!! Form::close() !!}  
                                
                    </div>                    
                </div>
            </div>
        </div>

<script type="text/javascript">
	$(".forgotpasswd").click(function(){
		$("#easylog").hide();
		$("#forgotlog").show();
	});
	
	$(".backlogin").click(function(){
		$("#easylog").show();
		$("#forgotlog").hide();
	});
</script>
@stop 
       