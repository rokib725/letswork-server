@extends('layouts.main')

@section('content')

<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">My Balance</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">My Balance</li>
                    </ol>
                </div>
                
            </div>
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">My Balance List</h4>
                                <h6 class="card-subtitle">Total Balance<code>10,000</code></h6>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Invoice No.</th>
                                                <th>Invoice for</th>
                                                <th>Invoice Date</th>
                                                <th>Due Date</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Invoice#01</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-danger">Not Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#02</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-info">Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#03</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-danger">Not Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#04</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-info">Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#05</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-danger">Not Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#06</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-info">Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#07</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-danger">Not Paid</span> </td>
                                            </tr>
                                            <tr>
                                                <td>Invoice#08</td>
                                                <td>Deshmukh</td>
                                                <td>19/10/2017</td>
                                                <td>Prohaska</td>
                                                <td>19/10/2017</td>
                                                <td><span class="label label-info">Paid</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


