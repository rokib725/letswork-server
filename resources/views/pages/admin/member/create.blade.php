@extends('layouts.main')

@section('content')

  <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid ">
  
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">New Member</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">New Member</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card card-body">
                        
                            <form class="form-horizontal">
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Name: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Email: <span class="help"> *</span></label>
                                        <input type="email" id="" name="email" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Phone: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Company: <span class="pull-right"><a href="#" title="Add Company"><i class="fa fa-plus-circle text-muted"></i></a></span></label>
                                        <select class="form-control custom-select" data-placeholder="Choose a Company" tabindex="1">
                                                <option value="Company 1">Company 1</option>
                                                <option value="Company 2">Company 2</option>
                                                <option value="Company 3">Company 5</option>
                                                <option value="Company 4">Company 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Address: </label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>                                    
                                </div> 
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <fieldset class="form-group">
                                        <label>Image</label>
                                        <label class="custom-file d-block">
                                            <input type="file" id="file" class="custom-file-input">
                                            <span class="custom-file-control"></span>
                                        </label>
                                    </fieldset>
                                </div>                              
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Package: </label>
                                        <select class="form-control custom-select" data-placeholder="Choose a Package" tabindex="1">
                                                <option value="Package 1">Package 1</option>
                                                <option value="Package 2">Package 2</option>
                                                <option value="Package 3">Package 5</option>
                                                <option value="Package 4">Package 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Role: </label>
                                        <select class="form-control custom-select" data-placeholder="Choose a Role" tabindex="1">
                                                <option value="Role 1">Role 1</option>
                                                <option value="Role 2">Role 2</option>
                                                <option value="Role 3">Role 5</option>
                                                <option value="Role 4">Role 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Joining Date: </label>
                                        <div class="input-group">
                                             <input type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy">
                                             <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-themecolor waves-effect waves-light m-r-10">Submit</button>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            
                            </form>                        
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


