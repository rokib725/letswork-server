@extends('layouts.main')

@section('content')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid ">
    
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Member</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Member</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body red-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body red-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body red-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body red-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body green-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- .col -->
                    <div class="col-md-4 col-lg-4 col-xlg-4">
                        <div class="card card-body red-lb">
                            <div class="row">
                                <div class="col-md-4 col-lg-3 text-center">
                                    <a href="member-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <a href="member-detail.html"><h3 class="box-title m-b-0">Johnathan Doe</h3></a> <small>Web Designer</small>
                                    <address>
                                        <a href="company-profile.html">Company</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


