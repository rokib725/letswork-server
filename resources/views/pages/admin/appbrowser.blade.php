@extends('layouts.main')

@section('content')

 <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid ">
   
  <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">App Browser</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">App Browser</li>
                    </ol>
                </div>
                
            </div>
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row app-browser">
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/Timeline.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">Timeline</p>                            
                            </a>
                            <p class="m-b-0"><small class="text-danger">Free</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/Calendar.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">Calendar</p>
                            </a>
                            <p class="m-b-0"><small class="text-danger">Free</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/freshbooks.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">freshbooks</p>
                            </a>
                            <p class="m-b-0"><small class="text-muted">$100</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/Paypal.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">Paypal</p>
                            </a>
                            <p class="m-b-0"><small class="text-danger">Free</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/Payford.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">Payford</p>                            
                            </a>
                            <p class="m-b-0"><small class="text-muted">$100</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-2 col-lg-2 col-xs-6">
                        <div class="card card-body">
                            <a href="#" class="text-center">
                            <figure><img src="{{ URL::asset('public/assets/images/Xero.png') }}" alt="" class="img-responsive"></figure>                            
                            <p class="card-subtitle">Xero</p>                            
                            </a>
                            <p class="m-b-0"><small class="text-muted">$100</small> <a href="#" class="pull-right"><small class="label label-light-inverse">View</small></a></p>
                        </div>
                    </div>
                    <!-- Column -->

                    
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@stop


