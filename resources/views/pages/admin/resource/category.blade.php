@extends('layouts.main')

@section('content')

 <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">New Resource Category</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">New Resource Category</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card card-body">
                        
                            <form class="form-horizontal" role="form" id="resource_category_form" method="POST" action="{{ url('resource/savecategory') }}">
                                {{ csrf_field() }}
                            <input type="hidden" name="categoryid" value="<?php echo isset($single_resource['categoryid'])==true?$single_resource['categoryid']:'0'; ?>">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Resource category Name: <span class="help"> * </span></label>
                                        <input type="text" class="form-control" name="category_name" id="category_name" value="<?php echo isset($single_resource['category_name'])==true?$single_resource['category_name']:''; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Resource category Detail: <span class="help"> * </span></label>
                                        <input type="text" class="form-control" name="category_detail" id="category_detail" value="<?php echo isset($single_resource['category_detail'])==true?$single_resource['category_detail']:''; ?>">
                                    </div>
                                </div>
                                                               
                            </div>

                            <button type="submit" class="btn btn-themecolor waves-effect waves-light m-r-10">Submit</button>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            
                            </form>  

                           
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="card-title">Resources categories</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Category name</th>
                                                    <th>category detail</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($resources as $key => $value) {
                                                   ?>
                                                <tr>
                                                    <td><?php echo $value['category_name'] ?></td>
                                                    <td><?php echo $value['category_detail'] ?></td>
                                                    <td>
                                                        <form method="POST" action="{{ url('resource/savecategory') }}" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="categoryid" value="<?php echo $value['categoryid'] ?>">
                                                        <input type="hidden" name="status" value="1">
                                                            <button type="submit" class="label label-info"><i class="fa fa-pencil"></i></button>

                                                        </form>
                                                        
                                                        <form method="POST" action="{{ url('resource/savecategory') }}" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="categoryid" value="<?php echo $value['categoryid'] ?>">
                                                        <input type="hidden" name="status" value="0">
                                                            <button type="submit" class="label label-danger"><i class="fa fa-trash-o"></i></button>

                                                        </form>
                                                        

                                                    </td>
                                                </tr>

                                                <?php } ?>                                                
                                                                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                      
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


