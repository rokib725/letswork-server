@extends('layouts.main')

@section('content')

<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Packages</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Packages</li>
                    </ol>
                </div>
                
            </div>
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-10 col-xlg-10 col-md-12">
                                        <h4 class="card-title">Packages List</h4>
                                        <h6 class="card-subtitle">Total Packages<code>100</code></h6>
                                    </div>
                                    <div class="col-lg-2 col-xlg-2 col-md-12">
                                        <a href="{{ url('/packages/create') }}" class="btn waves-effect waves-light btn-block btn-info">Add New</a>
                                    </div>
                                </div>
                                
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Title</th>
                                                <th>Branch Access</th>
                                                <th>Resource Category</th>
                                                <th>Monthly Price</th>
                                                <th>Free Use</th>
                                                <th>Paid Use</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>01</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td>02</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td>03</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td>04</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td>05</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td>06</td>
                                                <td>Packages title</td>
                                                <td>Dhaka, Chittagong</td>
                                                <td>Meeting Room 1</td>
                                                <td>1000 AED</td>
                                                <td>10 hours/Month</td>
                                                <td>30 hours/Month</td>
                                                <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


