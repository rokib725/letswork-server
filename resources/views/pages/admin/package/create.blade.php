@extends('layouts.main')

@section('content')

 <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">New Branches</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">New Branches</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card card-body">
                        
                            <form class="form-horizontal">
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Branch Name: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Branch Manager Name: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Phone: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Email: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Address: </label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </div>                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="card-title">Branch Resources</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Number</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Dedicated Desk</td>
                                                    <td>500</td>
                                                    <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Open Space</td>
                                                    <td>15</td>
                                                    <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Open Space</td>
                                                    <td>500</td>
                                                    <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dedicated Office</td>
                                                    <td>100</td>
                                                    <td>
                                                        <a href="#"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>
                                                        <a href="#"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1">
                                                            <option value="Category 1">Dedicated Desk</option>
                                                            <option value="Category 2">Open Space</option>
                                                            <option value="Category 3">Dedicated Office</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="500">
                                                    </td>
                                                    <td><button type="button" class="btn waves-effect waves-light btn-success"><i class="fa fa-plus"></i> Add</button></td>
                                                </tr>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-themecolor waves-effect waves-light m-r-10">Submit</button>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            
                            </form>                        
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


