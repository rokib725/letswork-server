@extends('layouts.main')

@section('content')

<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Branches</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ 'home' }}">Home</a></li>
                        <li class="breadcrumb-item active">Branches</li>
                    </ol>
                </div>
                
            </div>
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-10 col-xlg-10 col-md-12">
                                        <h4 class="card-title">Branches List</h4>
                                        <h6 class="card-subtitle">Total Branches<code><?php echo count($branches); ?></code></h6>
                                    </div>
                                    <div class="col-lg-2 col-xlg-2 col-md-12">
                                        <a href="{{ url('/branches/create') }}" class="btn waves-effect waves-light btn-block btn-info">Add New</a>
                                    </div>
                                </div>
                                
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Branch Name</th>
                                                <th>Branch Manager Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Branch Resources</th>
                                                <th>Number</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($branches as $key => $value) {
                                              
                                             ?>
                                            <tr>
                                                <td><?php echo $value['branch_id'] ?></td>
                                                <td><?php echo $value['branch_name'] ?></td>
                                                <td><?php echo $value['manager_name'] ?></td>
                                                <td><?php echo $value['phone'] ?></td>
                                                <td><?php echo $value['email'] ?></td>
                                                <td><?php echo $value['address'] ?></td>
                                                <td>Dedicated Desk</td>
                                                <td>100</td>
                                                <td>
                                                        <form method="POST" action="{{ url('branches/edit') }}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="branch_id" value="<?php echo $value['branch_id'] ?>">
                                                            <button type="submit" class="label label-info"><i class="fa fa-pencil"></i></button>

                                                        </form>
                                                    <form method="POST" action="{{ url('branches/delete') }}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="branch_id" value="<?php echo $value['branch_id'] ?>">

                                                        <button type="submit"  class="label label-danger"><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                    </td>

                                            </tr>

                                        <?php } ?>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@stop


