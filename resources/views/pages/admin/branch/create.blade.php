@extends('layouts.main')

@section('content')

 <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">New Branches</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">New Branches</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card card-body">
                        
                            <form class="form-horizontal" role="form" id="" method="POST" action="{{ url('branches/save') }}">
                                {{ csrf_field() }}
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Branch Name: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Branch Manager Name: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" name="manager_name" id="manager_name" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Phone: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" name="phone" id="phone" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Email: <span class="help"> *</span></label>
                                        <input type="text" class="form-control" id="email" name="email" value="">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Address: </label>
                                        <textarea class="form-control" rows="3" name="address" id="address"></textarea>
                                    </div>
                                </div>                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="card-title">Branch Resources</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="resources_table">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Number</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <tr>
                                                    <td>
                                                        <select class="form-control custom-select" data-placeholder="Choose a Category" id="resource_select" tabindex="1">
                                                        <?php foreach ($resources as $key => $value) {
                                                         ?> 
                                                            <option value="<?php echo $value->categoryid; ?>"><?php echo $value->category_name; ?></option>
                                                           
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="resource_quantity" class="form-control" value="">
                                                    </td>
                                                    <td><button type="button" onclick="add_resource_category_to_branch()" class="btn waves-effect waves-light btn-success"><i class="fa fa-plus"></i> Add</button></td>
                                                </tr>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-themecolor waves-effect waves-light m-r-10">Submit</button>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            
                            </form>                        
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <script type="text/javascript">
                function add_resource_category_to_branch() {
                    //alert($(element).val());
                    console.log($("#tr_"+$('#resource_select').val()).length);
                    if($("#tr_"+$('#resource_select').val()).length==0)
                    {
                     if ($("#resource_quantity").val()=='') {
                        alert('Empty quantity');
                        $("#resource_quantity").focus();
                        return;
                     }
                    var tr='<tr id="tr_'+$('#resource_select').val()+'">';                    
                        tr+='<td>'+$("#resource_select option:selected").text()+'</td>';
                        tr+='<td><input name="resource_category['+$('#resource_select').val()+']" readonly="readonly" id="quantity_'+$('#resource_select').val()+'" value="'+$("#resource_quantity").val()+'"/></td>';
                        tr+='<td>';
                          tr+='<a class="edit_category" tr_id='+$('#resource_select').val()+' href="javascript:void(0)"><span class="label label-info"><i class="fa fa-pencil"></i></span></a>';
                            tr+='<a class="remove_category" tr_id='+$('#resource_select').val()+' href="javascript:void(0)"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a>';
                        tr+='</td>';
                    tr+='</tr>';
                    $("#resources_table tbody").append(tr);
                    $("#resource_quantity").val('');
                  }
                  else
                  {
                    alert('Already added');
                  }
                }

            $('body').on('click','.edit_category',function() {
                var id=$(this).attr('tr_id');
                $("#quantity_"+id).removeAttr('readonly').focus();
            });
             $('body').on('click','.remove_category',function() {
                var id=$(this).attr('tr_id');
                $("#tr_"+id).remove();
            });

            </script>

@stop


