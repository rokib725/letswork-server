<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="">
    <meta name="robots" content="noindex" />
    <meta name="robots" content="nofollow" />

    <title>Entromizer</title><link href="{{ URL::asset('entromizer-ui/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('entromizer-ui/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('entromizer-ui/css/light-bootstrap-dashboard.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('entromizer-ui/css/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('entromizer-ui/css/timepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('entromizer-ui/css/demo.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/css/jquery-te.css') }}">
    
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ URL::asset('entromizer-ui/css/pe-icon-7-stroke.css') }}">
     <script src="{{ URL::asset('entromizer-ui/js/jquery-1.12.4.js') }}" type="text/javascript"></script>
      <script src="{{ URL::asset('entromizer-ui/js/jquery.blockUI.js') }}" type="text/javascript"></script>
      <script src="{{ URL::asset('entromizer-ui/js/jquery-ui.js') }}"></script>
      <script src="{{ URL::asset('public/js/jquery-te.min.js') }}"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
    
      
      
      
   <style>
   	
   	.ui-draggable, .ui-droppable {
	background-position: top;
}
.ui-autocomplete-loading {
		background: white url("{{ URL::asset('entromizer-ui/img/busy.gif') }}") right center no-repeat;
	}

   </style>   
      
    
   

</head>
