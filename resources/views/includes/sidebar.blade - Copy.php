<div class="sidebar" data-image="{{ URL::asset('public/entromizer-ui/img/sidebar-5.jpg') }}">

<div class="sidebar-wrapper">
    <div class="logo">
        <a href="{{ url('/') }}">
            <img src="{{ URL::asset('public/entromizer-ui/img/Logo.jpg') }}" alt="">
        </a>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="{{ url('/') }}">
                <i class="pe-7s-graph"></i>
                <p>Dashboard</p>
            </a>
        </li>
        
        <?php
        if (session()->has('menus'))
         {
         $all_menues=get_object_vars(json_decode(session('menus')));
        // print_r($all_menues);
         //exit;
         $main_menu=array_keys($all_menues);
         foreach($main_menu as $m)
         {
		 	$all_sub_menues=get_object_vars($all_menues[$m]);
		 	//print_r($all_sub_menues);
		 	if(count($all_sub_menues)>1)
		 	{
		 	?>
		 	<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-science"></i>
                <p><?php echo $m; ?> <b class="caret"></b></p>
            </a>
		 	<ul class="dropdown-menu">
		 	<?php	
		 	
			 foreach($all_sub_menues as $key=>$as)
			 {
			 	
			 	?>
			 	
                <li><a href="<?php echo url('/').'/'.$as->method."/".$key; ?>"><?php echo $as->display_name; ?></a></li>
                
			 	<?php
			 }
			 ?>
			 </ul>
        </li>
			 <?php	
			}else
			{
				$first_key = key($all_sub_menues);
				?>
				 <li>
            <a href="<?php echo url('/').'/'.$all_sub_menues[$first_key]->method; ?>">
                <i class="pe-7s-user"></i>
                <p><?php echo $m; ?></p>
            </a>
        </li>
				
				<?php
			}
		 }
         
         }
        	
        
        ?>
        


    </ul>
</div>
</div>