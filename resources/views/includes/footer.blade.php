<footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <!--<li>
                            <a href="#">
                                @lang('common.field_home')
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                @lang('common.field_company')
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                @lang('common.field_portfolio')
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               @lang('common.field_blog')
                            </a>
                        </li>-->
                        <li><a href="http://www.royex.ae" target="_blank">Developed by Royex Technologies</a></li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                <input  type="hidden" name="baseurl" id="baseurl" value="{{ url('/') }}"/>
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Entromizer</a>
                </p>
            </div>
        </footer>

<script src="{{ URL::asset('entromizer-ui/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('entromizer-ui/js/bootstrap-checkbox-radio-switch.js') }}"></script>
<script src="{{ URL::asset('entromizer-ui/js/chartist.min.js') }}"></script>
<script src="{{ URL::asset('entromizer-ui/js/bootstrap-notify.js') }}"></script>
<script src="{{ URL::asset('entromizer-ui/js/light-bootstrap-dashboard.js') }}"></script>

<script src="{{ URL::asset('entromizer-ui/js/jquery.timepicker.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	$('a.ara-layout').click(function(event) {
	  $("body").addClass("multi-layout");
	}); 
	
	$('a.eng-layout').click(function(event) {
	  $("body").removeClass("multi-layout");
	}); 
	
	
	$('#printpage').click(function(event) {
    event.preventDefault();
  var strFrameName = ("printer-" + (new Date()).getTime());
 

	var jFrame = $( "<iframe name='" + strFrameName + "'>" );
 

	jFrame
		.css( "width", "1px" )
		.css( "height", "1px" )
		.css( "position", "absolute" )
		.css( "left", "-9999px" )
		.appendTo( $( "body:first" ) )
	;
 
	// Get a FRAMES reference to the new frame.
	var objFrame = window.frames[ strFrameName ];
 
	// Get a reference to the DOM in the new frame.
	var objDoc = objFrame.document;
 
	
	var jStyleDiv = $( "<div>" ).append(
		$( "style" ).clone()
		);
 
	
	objDoc.open();
	/*objDoc.write( "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" );*/
	objDoc.write( "<html>" );
	objDoc.write( "<head>" );
	objDoc.write( "<title>" );
	objDoc.write( "" );
	objDoc.write( "</title>" );
	objDoc.write( "</head>" );
	objDoc.write( "<body><div class=\"content\"><div class=\"container-fluid\">" );
	var divContents = $("#printdiv").html();
	objDoc.write( divContents );
	objDoc.write( "</div></div></body>" );
	objDoc.write( "</html>" );
	objDoc.close();
 
	// Print the document.
	/*objFrame.focus();
	objFrame.print();
	*/
	objFrame.document.close();
objFrame.focus();
objFrame.print();
objFrame.close();
});

});
</script>
    