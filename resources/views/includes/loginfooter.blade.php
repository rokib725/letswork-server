<footer class="footer loginfooter">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Royex Technologies</a>
                </p>
                <input  type="hidden" id="baseurl" name="baseurl" value="{{ url('/') }}"/>
            </div>
        </footer>
<script src="{{ URL::asset('public/entromizer-ui/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('public/entromizer-ui/js/bootstrap-checkbox-radio-switch.js') }}"></script>
<script src="{{ URL::asset('public/entromizer-ui/js/chartist.min.js') }}"></script>
<script src="{{ URL::asset('public/entromizer-ui/js/bootstrap-notify.js') }}"></script>
<script src="{{ URL::asset('public/entromizer-ui/js/light-bootstrap-dashboard.js') }}"></script>
<script src="{{ URL::asset('public/entromizer-ui/js/jquery-ui.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
 
