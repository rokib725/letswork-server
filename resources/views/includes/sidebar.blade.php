
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="user-profile"> <a href="{{ url('/member/details') }}"><img src="{{ URL::asset('public/assets/images/users/profile.png') }}" alt="user" /><span class="hide-menu">John Henry </span></a>
                        </li>
                        <li class="nav-devider"></li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('/home') }}" aria-expanded="false"><i class="fa fa-th-large"></i><span class="hide-menu">Dashboard </span></a>                            
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('/timeline') }}" aria-expanded="false"><i class="mdi mdi-chart-timeline"></i><span class="hide-menu">Timeline</span></a>
                        </li>
                        
                        <li> <a class="waves-effect waves-dark" href="{{ url('/resource') }}" aria-expanded="false"><i class="ti-layers-alt"></i><span class="hide-menu">Resource</span></a>
                        </li>
                        
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Member </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/member/all') }}">All Member</a></li>
                                <li><a href="{{ url('/member/create') }}">New Member</a></li>                              
                            </ul>
                        </li>
                        
                        <li> <a class="waves-effect waves-dark" href="{{ url('/calendar/calendar') }}" aria-expanded="false"><i class="fa fa-calendar"></i><span class="hide-menu">Calendar</span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-email-outline"></i><span class="hide-menu">Message</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/message/inbox') }}">Inbox</a></li>
                                <li><a href="{{ url('/message/details') }}">Message Details</a></li>
                                <li><a href="{{ url('/message/compose') }}">Compose</a></li>
                            </ul>
                        </li>
                        <li class="nav-small-cap">Admin</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-docs"></i><span class="hide-menu">Reports </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="member-activity.html">Member Activity</a></li>
                                <li><a href="finance.html">Finance</a></li>
                                <li><a href="attendence.html">Attendance</a></li>                                
                            </ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('/app-browser') }}" aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">App Browser</span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/resource/savecategory') }}">Resource Category</a></li>
                                <li><a href="{{ url('/packages') }}">Packages</a></li>
                                <li><a href="{{ url('/branches') }}">Branches</a></li>                                
                            </ul>
                        </li>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->