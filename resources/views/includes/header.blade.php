 <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo "".Session::get('companyname'); ?></a>
                </div>
                <div class="collapse navbar-collapse">
                    
                    <ul class="nav navbar-nav navbar-right">
                    
                    @if(session()->has('userallcompany'))
                    <?php
                   $allcompany=Session::get('userallcompany');
                  
                    $companies=json_decode($allcompany);
                    ?>
    <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										<?php echo "".Session::get('companyname'); ?>
										<b class="caret"></b>
									</p>
                              </a>
                              <ul class="dropdown-menu">
                              <?php
                              foreach($companies as $key=>$value)
                              {
							  	
							  
                              ?>
                                <li><a href="<?php echo url("company/".$key."/switchCompany") ?>"><?php echo $value ?> <!--<span class="pull-right"><img src="../entromizer-ui/img/icons/usa.png" alt=""></span>--></a></li>
                                
                                
                                <?php
                                }
                                ?>
                              </ul>
                        </li>

@endif
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										@lang('common.language')
										<b class="caret"></b>
									</p>
                              </a>
                              <ul class="dropdown-menu" style="min-width: 105px;">
                                <li><a href="{{ url('/setlocale/en') }}" class="eng-layout">ENG <span class="pull-right"><img src="../entromizer-ui/img/icons/usa.png" alt=""></span></a></li>
                                <li><a href="{{ url('/setlocale/bd') }}" class="ara-layout">ARB <span class="pull-right"><img src="../entromizer-ui/img/icons/arabic.png" alt=""></span></a></li>
                              </ul>
                             
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										@lang('common.field_fiscalyear')<?php echo " ".Session::get('currentyear'); ?>
										<b class="caret"></b>
									</p>
                              </a>
                               @if(session()->has('fiscalyear'))
                              <ul class="dropdown-menu">
                               <?php
                   $allyear=Session::get('fiscalyear');
                  
                    $allyears=json_decode($allyear);
                    foreach($allyears as $key=> $a)
                    {
						
					
                    ?>
                                <li><a href="<?php echo url("company/".$key."/switchYear") ?>">@lang('common.field_year') : <?php echo $a; ?></a></li>
                                <?php
                                }
                                ?>
                               
                              </ul>
                              @endif
                        </li>
                        
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										@lang('common.field_loginasadmin')
										<b class="caret"></b>
									</p>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">@lang('common.field_profile')</a></li>
                                <li><a href="#">@lang('common.field_logfile')</a></li>
                              </ul>
                        </li>
                        
                        <li>
                           <a href="{{ url('/logout') }}">
                                <p>@lang('common.field_logout')</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>