<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Auth\LoginController@welcome');
Route::get('/login', 'Auth\LoginController@welcome');

Route::post('login',['as'=>'auth.login','uses'=>'Auth\LoginController@postLogin']);
Route::get('/home', 'AdminController@dashboard')->name('home');
Route::get('timeline',['as'=>'timeline','uses'=>'TimelineController@timeline']);
Route::post('timeline',['as'=>'timeline','uses'=>'TimelineController@timeline']);
Route::get('resource',['as'=>'resource','uses'=>'ResourceController@showAll']);
Route::post('resource',['as'=>'resource','uses'=>'ResourceController@showAll']);

Route::post('resource/savecategory',['as'=>'resource.savecategory','uses'=>'ResourceController@saveResourceCategory']);
Route::get('resource/savecategory',['as'=>'resource.savecategory','uses'=>'ResourceController@saveResourceCategory']);

Route::get('member/create',['as'=>'member.create','uses'=>'MemberController@createNew']);
Route::get('member/all',['as'=>'member.all','uses'=>'MemberController@showAll']);
Route::get('member/details',['as'=>'member.details','uses'=>'MemberController@memberDetail']);
Route::get('calendar/calendar',['as'=>'calendar.calendar','uses'=>'CalendarController@calendar']);
Route::get('message/compose',['as'=>'message.compose','uses'=>'MessageController@composeMessage']);
Route::get('message/inbox',['as'=>'message.inbox','uses'=>'MessageController@inboxMessage']);
Route::get('message/details',['as'=>'message.details','uses'=>'MessageController@messageDetails']);

Route::get('/app-browser',['as'=>'app-browser','uses'=>'AdminController@appbrowser']);
Route::get('/branches',['as'=>'branches','uses'=>'BranchController@showAll']);
Route::get('/branches/create',['as'=>'branches.create','uses'=>'BranchController@createNew']);
Route::post('branches/save',['as'=>'branches.save','uses'=>'BranchController@saveNew']);
Route::post('branches/edit',['as'=>'branches.edit','uses'=>'BranchController@editBranch']);
Route::post('branches/update',['as'=>'branches.update','uses'=>'BranchController@updateBranch']);
Route::post('branches/delete',['as'=>'branches.delete','uses'=>'BranchController@deleteBranch']);


Route::get('/packages',['as'=>'packages','uses'=>'PackageController@showAll']);
Route::get('/packages/create',['as'=>'packages.create','uses'=>'PackageController@createNew']);
Route::get('/member/balance',['as'=>'member.balance','uses'=>'MemberController@memberBalance']);